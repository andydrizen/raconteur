For generations we've communicated by analogy, metaphor and story. Your ability to convey your thoughts and ideas to others - whether it be software or otherwise - will have a huge impact on your success. Although there will always be early adopters, most of us will not be tempted by a new product without understanding - and connecting with - *your story*. 

**Raconteur is a powerful story-telling engine that enables you to transform stories into an interactive and charming engagement with your audience.**

You've spent time building your product, now help others understand it the way you do.

# Raconteur

1. / **noun** / One who tells anecdotes in a skillfull and amusing way.

## Getting Started

In what follows, you'll discover a set of tools that will help you unfold an engaging story to your audience. We will be discussing how Raconteur might be used in **gameplay**, but these concepts easily apply to other use cases, such as **onboarding** or **user retention**.

### 1. Setting Up Your Xcode Project

Whether or not you already have an project, getting started with Raconteur is easy. Simply download the framework and drag it into your project. Make sure that the Raconteur framework is listed in the "Embedded Binaries" and "Linked Frameworks and Libraries" in your project's "General" settings.

### 2. Create Your Story File

Create an empty text file called `story.txt` and drag it into your asset catalogue[^1]. 

In an appropriate place (perhaps your root view controller), initiate the a `Story` object with your text file, like so:

```
let story = Story(name: "story")
story.delegate = self
```

You'll notice that we've also assigned a delegate, so you'll need to conform to the `StoryProtocol` interface. This delegate will receive **lifecycle** and story **event** information. 

The **lifecycle callbacks** let you know when the story begins and ends. This might be used to modify your UI when these events occur. For example, you may display a "Game Over" screen to the reader upon conclusion of the story.

* `storyDidBegin(story: Story)`
* `storyDidEnd(story: Story)`

The **event callbacks** are discussed later.

Your story will be told passage by passage. Each passage must have a name - the first of which is called `start`. To get started, copy the following into your `story.txt` file

```
:: Start
Where am I?
My head is throbbing.
It's really dark, I can barely see the room around me.
<<choice [[Call for help|callforhelp]]>><<choice [[Try to find the light switch|findlightswitch]]>>

:: callforhelp
Hello?
Is anyone here..!?
[delay 1m|noise]

:: findlightswitch
OK, that makes sense...
Give me a minute.
[delay 1m|cuthand]

:: cuthand
<<set $cuthand = 1>>
Ouch, I cut my hand on something sharp!

:: noise
I heard a noise...
Someone is coming!

```

As you can see, each passage begins with two colons (::) a space and then the unique passage ID. Following the passage ID is a list of events that will be performed and then a newline to separate the passages. Let's take a look at the different types of event in more detail.

### 3. Events

Here is a list of every event you can perform within any passage. There must only be one event per line in your story text file.

#### State

To set or update the story state, you can use the `set` command, like so:

``<<set $happiness = 75>>``

or 

``<<set $happiness = $happiness + 10>>``

Every time this event is triggered, the `Story` object will inform its delegate using the method:

* `storyStateChanged(story: Story, oldValue : AnyObject?, newValue : AnyObject)`

#### Dialogue

For this event, the dialogue will be narrated by the story. Note that you can display your state in the dialogue, like so:

`My happiness is about $happiness percent right now.`

Every time this event is triggered, the `Story` object will inform its delegate using the method:

* `storyBeginDialogue(story: Story, dialogue: Dialogue)`

However, as you may be interested in simulating transmission time, you will also receive another callback when the dialogue event ends:

* `storyEndDialogue(story: Story, dialogue: Dialogue)`

The duration of the dialogue can be found in the `dialogue.duration` property.

#### Question

You can present questions using the following structure:

`<<choice [[DISPLAYED TEXT|PASSAGE NAME]]>>`

The "DISPLAYED TEXT" is what will be shown as an answer to the question, and the "PASSAGE NAME" is the passage that will be narrated next if this choice is chosen. 

If your question has multiple choices, you should add multiple choices on the same line, for example:

`<<choice [[Go north|north]]>><<choice [[Go South|south]]>>`

You can display your state in each choice, like this:

`<<choice [[Increase to $happiness|yes]]>><<choice [[Don't bother.|no]]>>`

Every time this event is triggered, the `Story` object will inform its delegate using the method:

* `storyQuestion(story: Story, question: Question)`

The `question.choices` array holds the available choices and to respond to the question, you should call the `Question` object's `respond()` function and pass in the selected choice. 

#### Delay

If you wish to delay the progression of the story, you can use this event. The structure is:

`[[delay 15m|PASSAGE NAME]]`

You may replace the `m` with `s` to delay by seconds instead of minutes. Once the delay is over, the passage named `PASSAGE NAME` will be narrated.

If you wish to have no delay, but wish to move to a different passage immediately, you may use the following shorthand:

`[[PASSAGE NAME]]`

Every time this event is triggered, the `Story` object will inform its delegate using the methods:

* `storyDelayDidBegin(story: Story, delay: Delay)`
* `storyDelayDidEnd(story: Story, delay: Delay)`

#### Audio

If you place a file called `singsong.mp3` into your asset catalogue, you can play it with the following event:

`<<audio singsong>>`

Every time this event is triggered, the `Story` object will inform its delegate using the method:

* `storyAudio(story: Story, audio: Audio)`

#### Condition

Raconteur has sophisticated conditional parsing which allows for the following:

* `<<if CONDITION>> ... <<endif>>`
* `<<if CONDITION>> ... <<else>> ... <<endif>>`
* `<<if CONDITION>> ... <<elseif CONDITION>> ... <<elseif CONDITION>> ... <<else>> ... <<endif>>`

It even supports nesting conditionals within conditionals.

For example:

```
<<if $candles eq 0>>
You've run out of candles
<<else>>
<<if $candles gte 100>>
I need to get rid of some of these candles anyway.
<<elseif $candles gte 10>>
Yeah, I don't need this many.
<<endif>>
I'm going to light one now.
[[LightACandle]]
<<endif>>
```

You can check your state in conditions, like so:

``<<if $happiness gte 70>>``

Possible operators within a conditional are:

* is, eq, ==
* neq, !=
* gt, >
* lt, <
* gte, >=
* lte, <=
* and, &&
* or, ||

#### Random

If you want to add some randomness to your dialogue, you can create events that all have the same prefix (such as "successmessage" and then have one of them chosen at random at runtime using `[[rand|successmessage]]`

#### Imagery

If you place a file called `myImage.png` into your asset catalogue, Raconteur can send a message to your delegate requesting it:

`<<image myImage>>`

Every time this event is triggered, the `Story` object will inform its delegate using the method:

* `storyImage(story: Story, image: Image)`

#### TextEntry

By using the following line in your story file, you can ask the user to input arbitrary text (e.g. ask for their name) and then move on to another passage.

`<<textentry|nextpassage>>`

#### Comments

Any lines that begin with `#` are treated as comments and are removed at parse time.

### 4. State

The Raconteur library will maintain state whilst the app is running, but it is your responsibility to persist this state between sessions. When you initiate the `Story` object, you'll pass in the passage to start from and state to use. You may like to look at the example project to see one possible implementation of this.



[^1]: As asset catalogues, are compiled during the build phase of your project, it is much harder for inquisitive minds to extract from the IPA than if we just added the text file into the bundle.

[^2]: Although it is possible to write your story in code using the Raconteur objects directly, it is preferable to use a text file stored in an asset catalogue to abstract it from Raconteur implementation details.