import XCTest
@testable import Raconteur

class ParserTests: XCTestCase {
    
    var excerpt : String?
    var story = Story(name: "")
    override func setUp() {
        super.setUp()
        guard let excerptPath = Bundle(for: type(of: self)).path(forResource: "excerpt", ofType: "txt") else {
            XCTFail("Could not load excerpt")
            return
        }
        excerpt =  try! NSString(contentsOfFile: excerptPath, encoding: String.Encoding.utf8.rawValue) as String
    }
    
    override func tearDown() {
        excerpt = nil
        super.tearDown()
    }
    
    func testDecomposeStoryFileIntoPassages() {
        
        let passages = Parser.decomposeStoryFileIntoPassages(excerpt!)
        XCTAssert(passages.count == 2)
        
    }
    
    func testDecomposePassageIntoLines() {
        
        let passages = Parser.decomposeStoryFileIntoPassages(excerpt!)
        let lines = Parser.decomposePassageIntoLines(passages[0])
        XCTAssert(lines.count == 3)
        
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }
    
    func testParseName() {
        let line = ":: Genesis"
        let name = Parser.parseName(line)
        
        XCTAssertNotNil(name)
        XCTAssertEqual(name, "Genesis")
    }
    
    func testParseDelayZero() {
        let line = "[[gonorth]]"
        let delay = Parser.parseZeroDelay(line)
        
        XCTAssertNotNil(delay)
        XCTAssertEqual(delay?.delay, 0)
        XCTAssertEqual(delay?.next, "gonorth")
    }
    
    func testParseDelaySeconds() {
        let line = "[[delay 31s|fightdragon]]"
        let delay = Parser.parseDelay(line)
        
        XCTAssertNotNil(delay)
        XCTAssertEqual(delay?.delay, 31)
        XCTAssertEqual(delay?.next, "fightdragon")
    }

    func testParseDelayMinutes() {
        let line = "[[delay 30m|haveanap]]"
        let delay = Parser.parseDelay(line)
        
        XCTAssertNotNil(delay)
        XCTAssertEqual(delay?.delay, 30 * 60)
        XCTAssertEqual(delay?.next, "haveanap")
    }

    
    func testParseState() {
        
        let line = "inline <<$happiness>> parsing <<$must>> work"
        let state = Parser.parseState(line, requireTags: true)
        
        XCTAssertNotNil(state)
        XCTAssertEqual(state, "inline [[happiness]] parsing [[must]] work")
    }
    
    func testParseChangeStateSingleString() {
        let line = "<<set $singular = \"\">>"
        let changeState = Parser.parseChangeState(line)
        
        XCTAssertNotNil(changeState)
        
        XCTAssert(changeState!.count == 1)
        
        let changeStateEvent = changeState![0]
        
        XCTAssertEqual(changeStateEvent.key, "singular")
        
        XCTAssertNotNil(changeStateEvent.value)
        
        guard let rawString = changeStateEvent.value as? String else {
            XCTFail("Could not parse value")
            return
        }
        XCTAssertEqual(rawString, "\"\"")
    }
    
    func testParseChangeStateSingleNumber() {
        let line = "<<set $singular = 42>>"
        let changeState = Parser.parseChangeState(line)
        
        XCTAssertNotNil(changeState)
        
        XCTAssert(changeState!.count == 1)
        
        let changeStateEvent = changeState![0]
        
        XCTAssertEqual(changeStateEvent.key, "singular")
        
        XCTAssertNotNil(changeStateEvent.value)
        
        guard let rawString = changeStateEvent.value as? String else {
            XCTFail("Could not parse value")
            return
        }
        XCTAssertEqual(rawString, "42")
    }
    
    func testParseAudio() {
        let line = "<<audio myfile>>"
        let event = Parser.parseAudio(line)
        
        XCTAssertNotNil(event)
        XCTAssertEqual(event!.name, "myfile")
    }
    
    func testParseImage() {
        let line = "<<image my_file>>"
        let event = Parser.parseImage(line)
        
        XCTAssertNotNil(event)
        XCTAssertEqual(event!.name, "my_file")
    }
    
    func testParseKeyboardEntry() {
        let line = "<<keyboardentry|default [[nextpassage]]>>"
        let event = Parser.parseKeyboardEntry(line)
        
        XCTAssertNotNil(event)
        XCTAssertNil(event?.question)
        XCTAssertEqual(event?.next, "nextpassage")
    }
    
    func testParseKeyboardEntryWithQuestion() {
        let line = "What's your name?<<keyboardentry|default [[nextpassage]]>>"
        let event = Parser.parseKeyboardEntry(line)
        
        XCTAssertNotNil(event)
        XCTAssertEqual(event?.question, "What's your name?")
        XCTAssertEqual(event?.keyboardType, UIKeyboardType.default)
        XCTAssertEqual(event?.next, "nextpassage")
    }
    
    func testParseEmailKeyboardEntryWithQuestion() {
        let line = "What's your name?<<keyboardentry|emailaddress [[nextpassage]]>>"
        let event = Parser.parseKeyboardEntry(line)
        
        XCTAssertNotNil(event)
        XCTAssertEqual(event?.question, "What's your name?")
        XCTAssertEqual(event?.keyboardType, UIKeyboardType.emailAddress)
        XCTAssertEqual(event?.next, "nextpassage")
    }
    
    func testParseChangeStateSingleBool() {
        let line = "<<set $singular = false>>"
        let changeState = Parser.parseChangeState(line)
        
        XCTAssertNotNil(changeState)
        
        XCTAssert(changeState!.count == 1)
        
        let changeStateEvent = changeState![0]
        
        XCTAssertEqual(changeStateEvent.key, "singular")
        
        XCTAssertNotNil(changeStateEvent.value)
        
        guard let rawString = changeStateEvent.value as? String else {
            XCTFail("Could not parse value")
            return
        }
        XCTAssertEqual(rawString, "false")
    }
    
    func testParseChangeStateToState() {
        let line = "<<set $singular = $double>>"
        let changeState = Parser.parseChangeState(line)
        
        XCTAssertNotNil(changeState)
        
        XCTAssert(changeState!.count == 1)
        
        let changeStateEvent = changeState![0]
        
        XCTAssertEqual(changeStateEvent.key, "singular")
        
        XCTAssertNotNil(changeStateEvent.value)
        
        guard let rawString = changeStateEvent.value as? String else {
            XCTFail("Could not parse value")
            return
        }
        XCTAssertEqual(rawString, "[[double]]")
    }
    
    func testParseChangeStateToRandNumber() {
        let line = "<<set $singular = rand(5)>>"
        let changeState = Parser.parseChangeState(line)
        
        XCTAssertNotNil(changeState)
        
        XCTAssert(changeState!.count == 1)
        
        let changeStateEvent = changeState![0]
        
        XCTAssertEqual(changeStateEvent.key, "singular")
        
        XCTAssertNotNil(changeStateEvent.value)
        
        guard let rawString = changeStateEvent.value as? String else {
            XCTFail("Could not parse value")
            return
        }
        XCTAssertEqual(rawString, "rand(5)")
    }
    
    func testParseChangeStateWithExtraSpaces() {
        let line = "<<set   $singular  =  rand(5)    >>"
        let changeState = Parser.parseChangeState(line)
        
        XCTAssertNotNil(changeState)
        
        XCTAssert(changeState!.count == 1)
        
        let changeStateEvent = changeState![0]
        
        XCTAssertEqual(changeStateEvent.key, "singular")
        
        XCTAssertNotNil(changeStateEvent.value)
        
        guard let rawString = changeStateEvent.value as? String else {
            XCTFail("Could not parse value")
            return
        }
        XCTAssertEqual(rawString, "rand(5)")
    }
    
    func testParseChangeStateToRandVar() {
        let line = "<<set $singular = rand($number)>>"
        let changeState = Parser.parseChangeState(line)
        
        XCTAssertNotNil(changeState)
        
        XCTAssert(changeState!.count == 1)
        
        let changeStateEvent = changeState![0]
        
        XCTAssertEqual(changeStateEvent.key, "singular")
        
        XCTAssertNotNil(changeStateEvent.value)
        
        guard let rawString = changeStateEvent.value as? String else {
            XCTFail("Could not parse value")
            return
        }
        XCTAssertEqual(rawString, "rand([[number]])")
    }
    
    func testParseChangeStateToRandVarPlusNumber() {
        let line = "<<set $singular = rand($number) + 4>>"
        let changeState = Parser.parseChangeState(line)
        
        XCTAssertNotNil(changeState)
        
        XCTAssert(changeState!.count == 1)
        
        let changeStateEvent = changeState![0]
        
        XCTAssertEqual(changeStateEvent.key, "singular")
        
        XCTAssertNotNil(changeStateEvent.value)
        
        guard let rawString = changeStateEvent.value as? String else {
            XCTFail("Could not parse value")
            return
        }
        XCTAssertEqual(rawString, "rand([[number]]) + 4")
    }
    
    func testParseChangeStateToRandVarPlusOtherVar() {
        let line = "<<set $singular = rand($number) + $otherVar>>"
        let changeState = Parser.parseChangeState(line)
        
        XCTAssertNotNil(changeState)
        
        XCTAssert(changeState!.count == 1)
        
        let changeStateEvent = changeState![0]
        
        XCTAssertEqual(changeStateEvent.key, "singular")
        
        XCTAssertNotNil(changeStateEvent.value)
        
        guard let rawString = changeStateEvent.value as? String else {
            XCTFail("Could not parse value")
            return
        }
        XCTAssertEqual(rawString, "rand([[number]]) + [[otherVar]]")
    }
    
    func testParseChangeStateTwoVars() {
        let line = "<<set $answer = \"$answer|$answerAlt\">>"
        let changeState = Parser.parseChangeState(line)
        XCTAssertNotNil(changeState)
        
        XCTAssert(changeState!.count == 1)
        
        let changeStateEvent = changeState![0]
        
        XCTAssertEqual(changeStateEvent.key, "answer")
        XCTAssertNotNil(changeStateEvent.value)
        
        guard let rawString = changeStateEvent.value as? String else {
            XCTFail("Could not parse value")
            return
        }
        XCTAssertEqual(rawString, "\"[[answer]]|[[answerAlt]]\"")
    }
    
    func testParseChangeStateMultipleString() {
        let line = "<<set $singular = \"abc\">> <<set $plural = \"def\">>"
        let changeState = Parser.parseChangeState(line)
        
        XCTAssertNotNil(changeState)
        
        XCTAssertEqual(changeState!.count, 2)
        
        let changeStateEvent0 = changeState![0]
        let changeStateEvent1 = changeState![1]
        
        XCTAssertEqual(changeStateEvent0.key, "singular")
        XCTAssertEqual(changeStateEvent1.key, "plural")
        
        XCTAssertNotNil(changeStateEvent0.value)
        XCTAssertNotNil(changeStateEvent1.value)
        
        guard let rawString0 = changeStateEvent0.value as? String else {
            XCTFail("Could not parse value")
            return
        }
        XCTAssertEqual(rawString0, "\"abc\"")
        
        guard let rawString1 = changeStateEvent1.value as? String else {
            XCTFail("Could not parse value")
            return
        }
        XCTAssertEqual(rawString1, "\"def\"")
    }
    
    func testParseQuestionWithQuestion() {
        let line = "What should I do?<<choice [[Keep going to the Varia.|keepgoingtovaria]]>><<choice [[Make Camp.|makecampatcaravel]]>>"
        let question = Parser.parseQuestion(line);
        
        XCTAssertNotNil(question)
        XCTAssert(question!.choices.count == 2)
        
        XCTAssertEqual(question?.question, "What should I do?")
        
        XCTAssertEqual(question!.choices[0].title, "Keep going to the Varia.")
        XCTAssertEqual(question!.choices[0].next, "keepgoingtovaria")
        
        XCTAssertEqual(question!.choices[1].title, "Make Camp.")
        XCTAssertEqual(question!.choices[1].next, "makecampatcaravel")
    }
    
    func testParseEmptyQuestion() {
        let line = "<<choice [[Keep going to the Varia.|keepgoingtovaria]]>><<choice [[Make Camp.|makecampatcaravel]]>>"
        let question = Parser.parseQuestion(line);
        
        XCTAssertNotNil(question)
        XCTAssert(question!.choices.count == 2)
        
        XCTAssertEqual(question?.question, nil)
        
        XCTAssertEqual(question!.choices[0].title, "Keep going to the Varia.")
        XCTAssertEqual(question!.choices[0].next, "keepgoingtovaria")
        
        XCTAssertEqual(question!.choices[1].title, "Make Camp.")
        XCTAssertEqual(question!.choices[1].next, "makecampatcaravel")
    }
 
    func testParseConditionElse() {
        let passageString = "<<if $glowrods is 1>><<set $plural = \"\">><<else>><<set $plural = \"s\">><<endif>>"
        let (_, _, condition) = Parser.prepareConditions(passageString, story:  story)
        
        XCTAssertNotNil(condition)
        XCTAssertEqual(condition?.condition, "[[glowrods]] is 1")
        
        XCTAssertEqual((condition?.success.events[0] as? ChangeState)?.key, "plural")
        XCTAssertEqual((condition?.success.events[0] as? ChangeState)?.value as? String, "\"\"")

        XCTAssertEqual((condition?.failure.events[0] as? ChangeState)?.key, "plural")
        XCTAssertEqual((condition?.failure.events[0] as? ChangeState)?.value as? String, "\"s\"")

    }
    
    func testParseConditionElseIf() {
        let passageString = "<<if $glowrods is 1>>Oh great!<<elseif $glowrods is 0>>Uh oh...<<elseif $glowrods lt 0>>How did this happen?<<endif>>"
        let (_, _,condition) = Parser.prepareConditions(passageString, story:  story)
        
        XCTAssertNotNil(condition)
        XCTAssertEqual(condition?.condition, "[[glowrods]] is 1")
        
        XCTAssertEqual((condition?.success.events[0] as? Dialogue)?.dialogue, "Oh great!")
        
        XCTAssertEqual((condition?.failure.events[0] as? Condition)?.condition, "[[glowrods]] is 0")
        XCTAssertEqual(((condition?.failure.events[0] as? Condition)?.success.events[0] as? Dialogue)?.dialogue, "Uh oh...")
        XCTAssertEqual(((condition?.failure.events[0] as? Condition)?.failure.events[0] as? Condition)?.condition, "[[glowrods]] lt 0")
        XCTAssertEqual((((condition?.failure.events[0] as? Condition)?.failure.events[0] as? Condition)?.success.events[0] as? Dialogue)?.dialogue, "How did this happen?")
        XCTAssertEqual(((condition?.failure.events[0] as? Condition)?.failure.events[0] as? Condition)?.failure.events.count, 0)
    }
    
    
    func testParseConditionElseIfElse() {
        let passageString = "<<if $glowrods is 1>>Oh great!<<elseif $glowrods is 0>>Uh oh...<<elseif $glowrods lt 0>>How did this happen?<<else>>Lots of glow sticks<<endif>>"

        let (_, _, IF) = Parser.prepareConditions(passageString, story: story)
        XCTAssertEqual(IF!.condition, "[[glowrods]] is 1")
        XCTAssertEqual((IF!.success.events[0] as? Dialogue)?.dialogue, "Oh great!")
        
        // Move on to ElseIf1
        let ELSEIF1 = (IF!.failure.events[0] as? Condition)!
        XCTAssertEqual(ELSEIF1.condition, "[[glowrods]] is 0")
        XCTAssertEqual((ELSEIF1.success.events[0] as? Dialogue)?.dialogue, "Uh oh...")
        
        // Move on to ElseIf2
        let ELSEIF2 = (ELSEIF1.failure.events[0] as? Condition)!
        XCTAssertEqual(ELSEIF2.condition, "[[glowrods]] lt 0")
        XCTAssertEqual((ELSEIF2.success.events[0] as? Dialogue)?.dialogue, "How did this happen?")
        
        // Finally, execute the ELSE section
        XCTAssertEqual((ELSEIF2.failure.events[0] as? Dialogue)?.dialogue, "Lots of glow sticks")
    }
    
    func testNestedIf() {
        let passageString = "<<if $glowrods is 1>>Oh great!<<if $happy is 1>>I'm happy with glowrods!<<endif>><<else>>Lots of glow sticks<<endif>>"
        
        let (_, events) = Parser.parsePassage(passageString, story:  story)
        
        
        let IF = events[0] as! Condition
        
        XCTAssertEqual(IF.condition, "[[glowrods]] is 1")
        XCTAssertEqual((IF.success.events[0] as? Dialogue)?.dialogue, "Oh great!")
        XCTAssertEqual((IF.failure.events[0] as? Dialogue)?.dialogue, "Lots of glow sticks")
    }
    
    func testSimpleIf() {
        let passageString = "<<if $toldname is 0>>My name's Taylor, by the way. Probably should've started with that.<<endif>>"
        
        let (_, events) = Parser.parsePassage(passageString, story:  story)
        
        
        let IF = events[0] as! Condition
        
        XCTAssertEqual(IF.condition, "[[toldname]] is 0")
        XCTAssertEqual((IF.success.events[0] as? Dialogue)?.dialogue, "My name's Taylor, by the way. Probably should've started with that.")
        XCTAssertEqual(IF.failure.events.count, 0)
    }
}

