import Foundation

public protocol StoryProtocol : class {
    func storyDidBegin(_ story: Story)
    func storyDidEnd(_ story: Story)
    
    func storyBeginDialogue(_ story: Story, dialogue: Dialogue)
    func storyEndDialogue(_ story: Story, dialogue: Dialogue)
    func storyQuestion(_ story: Story, question: Question)
    func storyAudio(_ story: Story, audio: Audio)
    func storyImage(_ story: Story, image: Image)
    func storyKeyboardEntry(_ story: Story, keyboardEntry: KeyboardEntry)
    func storyStateChanged(_ story: Story, state: ChangeState, oldValue : AnyObject?, newValue : AnyObject)
    func storyDelayDidBegin(_ story: Story, delay: Delay)
    func storyDelayDidEnd(_ story: Story, delay: Delay)
}

public extension StoryProtocol {
    func storyDidBegin(_ story: Story) {}
    func storyDidEnd(_ story: Story) {}
    
    func storyBeginDialogue(_ story: Story, dialogue: Dialogue) {}
    func storyEndDialogue(_ story: Story, dialogue: Dialogue) {}
    func storyQuestion(_ story: Story, question: Question) {}
    func storyAudio(_ story: Story, audio: Audio) {}
    func storyImage(_ story: Story, image: Image) {}
    func storyKeyboardEntry(_ story: Story, keyboardEntry: KeyboardEntry) {}
    func storyStateChanged(_ story: Story, state: ChangeState, oldValue : AnyObject?, newValue : AnyObject) {}
    func storyDelayDidBegin(_ story: Story, delay: Delay) {}
    func storyDelayDidEnd(_ story: Story, delay: Delay) {}
}

open class Story: NSObject, DialogueProtocol, QuestionProtocol, DelayProtocol,
ChangeStateProtocol, AudioProtocol, ImageProtocol, KeyboardEntryProtocol, RandomProtocol {
    
    //MARK: Story Setup
    
    open weak var delegate : StoryProtocol?
    open var firstPassageName : String?
    open var storyState : [String : AnyObject] = [:]
    open var passages = Passages()
    open var printDebugLogs : Bool {
        get {
            return Log.printDebugLogs
        }
        set {
            Log.printDebugLogs = newValue
        }
    }
    
    public init(name: String) {
        super.init()
        guard
            let storyData = NSDataAsset(name: name)?.data,
            let contents = NSString(data: storyData, encoding: String.Encoding.utf8.rawValue) else {
            assert(false, "Story could not be parsed.")
        }
        
        Parser.parse(self, storyFile: String(describing:contents))
    }
    
    open func addPassage(_ name: String?, events: [Event]) -> Passage {
        let passage = Passage(name: name, events: events, eventDelegate: self)
        passages.add(passage)
        passage.deadEndHandler = { [unowned self] in
            self.delegate?.storyDidEnd(self)
        }
        return passage
    }
    
    deinit {
        
        // It's possible that some events were sheduled to act in the future
        // (e.g. dialogue) so we cancel their future requests here.
        for e in passages.allEvents {
            Story.cancelPreviousPerformRequests(withTarget: e)
        }
        
        Log.Debug("Story has been deinitialised.")
    }
    
    //MARK: Story Lifecycle
    
    open func startStory(fromEvent eventID : Int?, storyState : [String : AnyObject]?) {
        delegate?.storyDidBegin(self)
        if storyState != nil {
            self.storyState = storyState!
        } else {
            self.storyState = [:]
        }
        
        if let eventID = eventID {
            if passages.allEvents.count - 1 >= eventID {
                passages.allEvents[eventID].end()
            }
        }
        else {
            guard let name = firstPassageName else {
                Log.Error("Error: First passage not set.")
                return
            }
            guard let passage = passages.passageNamed(name) else {
                Log.Error("Error: Could not find first passage.")
                return
            }
            
            guard let event = passage.events.first else { return }
            event.begin()
        }
    }
    
    open func startStory(fromPassage passage: String?, storyState : [String : AnyObject]?) {
        delegate?.storyDidBegin(self)
        if storyState != nil {
            self.storyState = storyState!
        } else {
            self.storyState = [:]
        }
        var passageName : String
        if let passage = passage {
            passageName = passage
        } else {
            guard let name = firstPassageName else {
                Log.Error("Error: First passage not set.")
                return
            }
            passageName = name
        }
        guard let passage = passages.passageNamed(passageName) else {
            Log.Error("Error: Could not find first passage.")
            return
        }
        
        guard let event = passage.events.first else { return }
        event.begin()
    }
    
    // MARK: Event Protocol
    
    func currentStateForEvent(_ event: Event) -> [String : AnyObject]? {
        return storyState
    }
    
    // MARK: Handle Dialogue
    
    func eventBeginDialogue(_ dialogue: Dialogue) {
        delegate?.storyBeginDialogue(self, dialogue: dialogue)
    }
    
    func eventEndDialogue(_ dialogue: Dialogue) {
        delegate?.storyEndDialogue(self, dialogue: dialogue)
    }
    
    // MARK: Handle Question
    
    func eventQuestion(_ question: Question) {
        delegate?.storyQuestion(self, question: question)
    }
    
    //MARK: Handle Delay
    
    func eventBeginDelay(_ delay: Delay) {
        delegate?.storyDelayDidBegin(self, delay: delay)
    }
    
    func eventEndDelay(_ delay: Delay) {
        delegate?.storyDelayDidEnd(self, delay: delay)
    }
    
    func eventAudio(_ audio: Audio) {
        delegate?.storyAudio(self, audio: audio)
    }
    
    func eventRandomPassageWithPrefix(_ random: Random, prefix: String) {
        let options = passages.passagesNamedWithPrefix(prefix)
        let randomIndex = Int(arc4random_uniform(UInt32(options.count)))
        guard let event = options[randomIndex].events.first else { return }
        event.begin()
    }
    
    //MARK: Handle Change State
    
    func eventChangeState(_ state: ChangeState) {
        let valueString = "\(state.value)".stringByReplacingStateTokens(storyState, quoteStrings: false)
        let parsedValue: AnyObject = NSExpression(format: valueString).expressionValue(with: nil, context: nil) as AnyObject
        
        if state.key.hasPrefix("system") {
            let oldValue = Persistence.objectForSystemKey(key: state.key)
            Persistence.setObject(value: parsedValue, systemKey: state.key)
            delegate?.storyStateChanged(self, state: state, oldValue: oldValue, newValue: parsedValue)
            return
        }
        
        let oldValue = objectForPlayerKey(key: state.key)
        setObject(value: parsedValue, playerKey: state.key)
        delegate?.storyStateChanged(self, state: state, oldValue: oldValue, newValue: parsedValue)
    }
    
    func eventImage(_ image: Image) {
        delegate?.storyImage(self, image: image)
    }
    
    func eventKeyboardEntry(_ keyboardEntry: KeyboardEntry) {
        delegate?.storyKeyboardEntry(self, keyboardEntry: keyboardEntry)
    }
    
}
