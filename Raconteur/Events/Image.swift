import Foundation

protocol ImageProtocol : EventProtocol {
    func eventImage(_ image: Image)
}


open class Image: Event {
    
    open var name: String
    
    init(_ name: String) {
        self.name = name
        super.init()
    }
    
    override open var description : String {
        get {
            return "Image: \(name)"
        }
    }
    
    override func begin() {
        super.begin()
        
        if let d = delegate as? ImageProtocol {
            d.eventImage(self)
        }
        
        end()
    }
    
    override func end() {
        super.end()
        completionHandler?(nil)
    }
}
