import Foundation

open class Condition: Event {
    var condition: String
    var failure: Passage
    var success: Passage
    
    
    init(_ condition : String, success: Passage, failure: Passage) {
        self.condition = condition
        self.success = success
        self.failure = failure
        super.init()
        self.success.events.forEach {$0.parent = self}
        self.failure.events.forEach {$0.parent = self}
    }
    
    override open var description : String {
        get {
            return "-- Condition: \(condition) --"
        }
    }
    
    override func begin() {
        super.begin()
        end()
    }
    
    override func end() {
        super.end()
        guard let state = delegate?.currentStateForEvent(self) else {
            Log.Error("Error: Could not read current state.")
            return
        }
        var parsedCondition = self.condition
        parsedCondition = parsedCondition.stringByReplacingStateTokens(state, quoteStrings: true)
        parsedCondition = parsedCondition.replacingOccurrences(of: " is ", with: " == ")
        parsedCondition = parsedCondition.replacingOccurrences(of: " eq ", with: " == ")
        parsedCondition = parsedCondition.replacingOccurrences(of: " neq ", with: " != ")
        parsedCondition = parsedCondition.replacingOccurrences(of: " gt ", with: " > ")
        parsedCondition = parsedCondition.replacingOccurrences(of: " lt ", with: " < ")
        parsedCondition = parsedCondition.replacingOccurrences(of: " gte ", with: " >= ")
        parsedCondition = parsedCondition.replacingOccurrences(of: " lte ", with: " <= ")
        parsedCondition = parsedCondition.replacingOccurrences(of: " and ", with: " && ")
        parsedCondition = parsedCondition.replacingOccurrences(of: " or ", with: " || ")
        
        let predicate = NSPredicate(format: parsedCondition)
        let result = predicate.evaluate(with: "")
        
        let passage = result ? success : failure
        passage.events.forEach {$0.delegate = self.delegate}
        
        if passage.events.count > 0 {
            passage.events.first?.begin()
        }
        else {
            completionHandler?(nil)
        }
    }
}
