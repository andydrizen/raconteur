import Foundation

protocol EventProtocol : class {
    func currentStateForEvent(_ event : Event) -> [String : AnyObject]?
}

open class Event : NSObject {
    
    // Upon calling this completion handler, the parent Story object will
    // perform the next item in the current passage unless you pass a new
    // passage name, in which case, the first item in the new passage
    // will be called.
    var completionHandler: ((_ passage : String?) -> (Void))?
    
    // All delegate calls will be passed to the delegate of the Story object
    weak var delegate: EventProtocol?
    
    weak var parent : Event?
    
    // This method will cause the event to be fired.
    func begin() {}
    func end() {}
}
