import Foundation

protocol KeyboardEntryProtocol : EventProtocol {
    func eventKeyboardEntry(_ keyboardEntry: KeyboardEntry)
}


open class KeyboardEntry: Event {
    
    open var question : String?
    open var next : String
    open var keyboardType : UIKeyboardType
    
    init(_ question: String?, next: String, keyboardType : UIKeyboardType) {
        self.question = question
        self.next = next
        self.keyboardType = keyboardType
        super.init()
    }
    
    override open var description : String {
        get {
            var str = "Q: "
            if question != nil {
                str = str + "\(question!)"
            } else {
                str = str + "-"
            }
            return str
        }
    }
    
    override func begin() {
        super.begin()
        end()
    }
    
    override func end() {
        super.end()
        if let d = delegate as? KeyboardEntryProtocol {
            d.eventKeyboardEntry(self)
        }
    }
    
    open func respond() {
        completionHandler?(next)
    }
}
