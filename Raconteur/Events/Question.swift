import Foundation

protocol QuestionProtocol : EventProtocol {
    func eventQuestion(_ question: Question)
}

open class Question: Event, NSCoding {
    
    open var choices : [Choice]
    open var question : String?
    
    open var selectedIndex : Int?
    
    override open var description : String {
        get {
            var str = "Q: "
            if question != nil {
                str = str + "\(question!)"
            } else {
                str = str + "-"
            }
            for choice in choices {
                str = str + "\t* \(choice.title)"
            }
            return str
        }
    }
    
    // We need this because variadics cannot accept arrays as of Swift 2.0 (splatting)
    // https://devforums.apple.com/message/970958
    // http://stackoverflow.com/questions/24024376/passing-an-array-to-a-function-with-variable-number-of-args-in-swift
    
    init(_ question : String?, choices : [Choice]) {
        self.question = question
        self.choices = choices
        super.init()
    }
    
    init(_ question : String?, choices : Choice...) {
        self.question = question
        self.choices = choices
        super.init()
    }

    required public init?(coder aDecoder: NSCoder) {
        question = aDecoder.decodeObject(forKey: "question") as? String
        choices = aDecoder.decodeObject(forKey: "choices") as! [Choice]
        selectedIndex = aDecoder.decodeObject(forKey: "selectedIndex") as? Int
    }
    
    public func encode(with aCoder: NSCoder) {
        aCoder.encode(question, forKey: "question")
        aCoder.encode(choices, forKey: "choices")
        aCoder.encode(selectedIndex, forKey: "selectedIndex")
    }
    
    override func begin() {
        super.begin()

        if let d = delegate as? QuestionProtocol {
            d.eventQuestion(self)
        }
    }

    override func end() {
        super.end()
    }
}

open class Choice : NSObject, NSCoding {
    open var title : String
    open var next : String?
    
    init(_ title: String, next: String?) {
        self.title = title
        self.next = next
        super.init()
    }
    
    required public init?(coder aDecoder: NSCoder) {
        title = aDecoder.decodeObject(forKey: "title") as! String
        next = aDecoder.decodeObject(forKey: "next") as? String
    }
    
    public func encode(with aCoder: NSCoder) {
        aCoder.encode(title, forKey: "title")
        aCoder.encode(next, forKey: "next")
    }
}
