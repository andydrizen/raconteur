import Foundation

protocol DialogueProtocol : EventProtocol {
    func eventBeginDialogue(_ dialogue: Dialogue)
    func eventEndDialogue(_ dialogue: Dialogue)
}

open class Dialogue: Event, NSCoding {

    open var dialogue : String
    open var wordsPerMinute : Int
    
    let minDuration = 0.5
    
    override open var description : String {
        get {
            return dialogue
        }
    }
    
    open var duration : TimeInterval {
        // Simulate the typing speed of the message
        let numberOfCharacters = dialogue.characters.count
        // Assume that, on average, words are 4 characters long.
        let secondsForFullMessage = max(minDuration, Double((Float(numberOfCharacters) / (4 * Float(wordsPerMinute))) * 60))
        return Double(secondsForFullMessage)
    }
    
    init(_ dialogue : String, wordsPerMinute : Int = 170) {
        self.dialogue = dialogue
        self.wordsPerMinute = wordsPerMinute
        super.init()
    }
    
    override func begin() {
        super.begin()
        
        if let d = delegate as? DialogueProtocol {
            d.eventBeginDialogue(self)
            let t = max(duration, minDuration)
            
            perform(.finishEvent, with: nil, afterDelay: TimeInterval(t))
        }
    }
    
    func finishEvent() {
        if let d = delegate as? DialogueProtocol {
            d.eventEndDialogue(self)
        }
        self.end()
    }
    
    override func end() {
        super.end()
        self.completionHandler?(nil)
    }
    
    public required init?(coder aDecoder: NSCoder) {
        dialogue = aDecoder.decodeObject(forKey: "dialogue") as? String ?? ""
        wordsPerMinute = aDecoder.decodeObject(forKey: "wordsPerMinute") as? Int ?? 170
        super.init()
    }
    
    public func encode(with aCoder: NSCoder) {
        aCoder.encode(wordsPerMinute, forKey: "wordsPerMinute")
        aCoder.encode(dialogue, forKey: "dialogue")
    }
}

extension Selector {
    static let finishEvent = #selector(Dialogue.finishEvent)
}
