import Foundation

protocol RandomProtocol : EventProtocol {
    func eventRandomPassageWithPrefix(_ random: Random, prefix : String)
}

open class Random: Event {
    
    var unparsedNextPrefix : String
    var nextPrefix: String {
        get {
            guard let state = delegate?.currentStateForEvent(self) else {
                Log.Error("Error: Could not read current state.")
                return unparsedNextPrefix
            }
            return unparsedNextPrefix.stringByReplacingStateTokens(state, quoteStrings: false)
        }
    }
    
    override open var description : String {
        get {
            return "-- Goto random passage: \(nextPrefix)* --"
        }
    }
    
    init(_ nextPrefix : String) {
        self.unparsedNextPrefix = nextPrefix
        super.init()
    }
    
    override func begin() {
        super.begin()
        end()
    }
    
    override func end() {
        super.end()
        if let d = delegate as? RandomProtocol {
            d.eventRandomPassageWithPrefix(self, prefix: nextPrefix)
        }
    }
}
