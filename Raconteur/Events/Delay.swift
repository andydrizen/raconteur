import Foundation

protocol DelayProtocol : EventProtocol {
    func eventBeginDelay(_ delay: Delay)
    func eventEndDelay(_ delay: Delay)
}



open class Delay: Event {

    open var delay : TimeInterval
    open var next : String?
    fileprivate var timer : Timer?
    fileprivate var endDate : Date?

    override open var description : String {
        get {
            if delay == 0 {
                return "-- Linking to passage: \(next!) --"
            } else {
                return "-- Delaying for \(delay) seconds --"
            }
        }
    }
    
    init(_ delay : TimeInterval, next : String? = nil) {
        self.delay = delay
        self.next = next
        super.init()
    }

    override func begin() {
        super.begin()
        end()
    }
    
    open func updateDelay(_ delay: TimeInterval) {
        endDate = Date().addingTimeInterval(delay)
        
        guard let endDate = endDate else { return }
        Log.Debug("Updating endDate to \(endDate)")
    }
    
    override func end() {
        super.end()
        
        if delay == 0 {
            completionHandler?(next)
        } else {
            endDate = Date().addingTimeInterval(delay)
            
            if let d = delegate as? DelayProtocol {
                d.eventBeginDelay(self)
            }
            
            startTimer()
        }
    }
    
    open func endDelay() {
        if let d = delegate as? DelayProtocol {
            d.eventEndDelay(self)
        }
        completionHandler?(next)
    }
    
    func shouldTimerExpire(_ futureDate : Date) -> Bool {
        return delay <= 0 || (futureDate as NSDate).laterDate(Date()) != futureDate
    }
    
    func startTimer() {
        timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: .handleTimer, userInfo: nil, repeats: true)
    }
    
    func handleTimer(_ timer : Timer) {
        guard let savedDate = endDate else {
            Log.Error("Error: No end date found")
            return
        }

        if(shouldTimerExpire(savedDate)){
            Log.Debug("-- Stopping timer: \(Date())")
            stopTimer(timer)
        }
    }
    
    func stopTimer(_ timer : Timer) {
        timer.invalidate()
        endDelay()
    }

}

extension Selector {
    static let handleTimer = #selector(Delay.handleTimer(_:))
}
