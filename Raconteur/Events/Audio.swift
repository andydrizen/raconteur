import Foundation

protocol AudioProtocol : EventProtocol {
    func eventAudio(_ audio: Audio)
}


open class Audio: Event {
    
    open var name: String
    
    init(_ name: String) {
        self.name = name
        super.init()
    }
    
    override open var description : String {
        get {
            return "Audio: \(name)"
        }
    }
    
    override func begin() {
        super.begin()
        
        if let d = delegate as? AudioProtocol {
            d.eventAudio(self)
        }
            
        end()
    }
    
    
    override func end() {
        super.end()
        completionHandler?(nil)
    }
}
