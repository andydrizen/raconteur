import Foundation

protocol ChangeStateProtocol : EventProtocol {
    func eventChangeState(_ state: ChangeState)
}


open class ChangeState: Event {
    
    open var key: String
    var value : AnyObject
    override open var description : String {
        get {
            return "-- Setting \(key) = \(value) --"
        }
    }
    
    init(_ key : String, value: AnyObject) {
        self.key = key
        self.value = value
        super.init()
    }
    
    override func begin() {
        super.begin()
        
        if let d = delegate as? ChangeStateProtocol {
            d.eventChangeState(self)
        }
        
        end()
    }
    
    override func end() {
        super.end()
        completionHandler?(nil)
    }
}
