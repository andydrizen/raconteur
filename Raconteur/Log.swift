import Foundation

struct Log {
    
    static var printDebugLogs = false
    
    static func Debug(_ message : Any...){
        if printDebugLogs {
            print(message)
        }
    }
    
    static func Error(_ message : Any...){
        print(message)
    }
}
