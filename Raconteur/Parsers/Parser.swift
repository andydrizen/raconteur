//
//  Racon.swift
//  Storytime
//
//  Created by Andy Drizen on 10/05/2015.
//  Copyright (c) 2015 Andy Drizen. All rights reserved.
//

import Foundation

class Parser: NSObject {
    
    static var processedEvents : [String : Event] = [:]
    
    class func importFiles(_ storyFile : String) -> String {
        
        let pattern = "<<import ([^>]+)>>"
        
        var newStoryFile = storyFile
        var importFound = false
        repeat {
            importFound = false
            let result = NSRegularExpression.firstMatchWithPattern(pattern, haystack: newStoryFile)
            if let result = result {
                importFound = true
                let globalRange = result.rangeAt(0)
                let range = result.rangeAt(1)
                let filename = (newStoryFile as NSString).substring(with: range)
                
                if let storyData = NSDataAsset(name: filename)?.data {
                    if let contents = NSString(data: storyData, encoding: String.Encoding.utf8.rawValue) {
                        newStoryFile = (newStoryFile  as NSString).replacingCharacters(in: globalRange, with: contents as String)
                    }
                }
            }
        } while importFound
        return newStoryFile
    }
    
    class func parse(_ story: Story, storyFile : String) {
        
        let storyFileWithImports = importFiles(storyFile)
        
        let allPassages = decomposeStoryFileIntoPassages(storyFileWithImports)
        
        for passage in allPassages {
            let (name, events) = parsePassage(passage, story: story)
            _ = story.addPassage(name, events: events)
        }
        processedEvents = [:]
        story.firstPassageName = "Start"
    }
    
    class func parsePassage(_ passage : String, story : Story ) -> (name: String?, events: [Event]){
        var newPassage = passage
        var latestCondition : Condition?
        
        repeat {
            let (parsedPassage, key, condition) = prepareConditions(newPassage, story: story)
            newPassage = parsedPassage
            latestCondition = condition
            if condition != nil {
                processedEvents[key!] = condition!
            }
        } while latestCondition != nil
        
        let lines = decomposePassageIntoLines(newPassage)
        var events : [Event] = []
        var name : String?
        
        for line in lines {
            if name == nil {
                if let passageName = parseName(line) {
                    name = passageName
                    continue
                }
            }
            for e in parseEvents(line, preprocessed: processedEvents) {
                events.append(e)
            }
        }
        return (name: name, events: events)
        
    }
    
    
    class func prepareConditions(_ passage : String, story : Story) -> (passage: String, conditionKey: String?, condition: Condition?) {
        let nsPassage = passage as NSString
        
        // Get the inner-most IF statemnet
        let pattern = "<<if ([^>]+)>>((?:.(?!<<if |<<endif>>))*.)(?:<<elseif |<<endif>>)"
        guard let match = NSRegularExpression.firstMatchWithPattern(pattern, haystack: passage) else { return (passage, nil, nil) }
        
        let start = nsPassage.substring(to: match.rangeAt(0).location)
        let conditionalKey = UUID().uuidString
        let end = nsPassage.substring(from: match.rangeAt(0).location + match.rangeAt(0).length)
        let newPassage = "\(start)\n<<conditional:\(conditionalKey)>>\n\(end)"
        
        // Get the intial condition
        let condition = nsPassage.substring(with: match.rangeAt(1))
        
        // Is there an ELSE?
        let body = nsPassage.substring(with: match.rangeAt(2)).components(separatedBy: "<<else>>")
        
        // Break up the section before the ELSE
        // if c1 [elseif c2] ... [elseif cN] [else] endif
        let clauses = body[0].components(separatedBy: "<<elseif ")
        
        var elseBranch : String?
        if body.count == 2 {
            elseBranch = body[1]
        }
        
        if clauses.count > 1 {
            var failures : [Passage] = []
            
            // get the else section
            var passageForElse : Passage
            if elseBranch != nil {
                let (nameElse, eventsElse) = parsePassage(elseBranch!, story: story)
                passageForElse = Passage(name: nameElse, events: eventsElse, eventDelegate: story)
            } else {
                passageForElse = Passage(name: nil, events: [], eventDelegate: story)
            }
            story.passages.add(passageForElse)
            failures.append(passageForElse)
            
            for index in 1..<clauses.count {
                let i = clauses.count - index
                let elseifBranch = clauses[i].componentsSeparatedByFirstString(">>")
                
                let elseifBranchCondition = elseifBranch[0]
                let elseifBranchBody = elseifBranch[1]
                
                let (nameElse, eventsElse) = parsePassage(elseifBranchBody, story: story)
                let passage0 = Passage(name: nameElse, events: eventsElse, eventDelegate: story)
                story.passages.add(passage0)
                let c = Condition(parseState(elseifBranchCondition, requireTags: false), success: passage0, failure: failures[index-1])
                let failureC = Passage(name: nil, events: [c], eventDelegate: story)
                story.passages.add(failureC)
                failures.append(failureC)
            }
            
            let ifBranch = clauses[0]
            let (nameIf, eventsIf) = parsePassage(ifBranch, story: story)
            let passageIf = Passage(name: nameIf, events: eventsIf, eventDelegate: story)
            story.passages.add(passageIf)
            let finalPassage = (failures.last)!
            
            return (newPassage, conditionalKey, Condition(parseState(condition, requireTags: false), success: passageIf, failure: finalPassage))
        }
            
            // No elseif
        else {
            let ifBranch = body[0]
            let (nameIf, eventsIf) = parsePassage(ifBranch, story: story)
            let passage0 = Passage(name: nameIf, events: eventsIf, eventDelegate: story)
            story.passages.add(passage0)
            
            var passage1 : Passage
            if elseBranch != nil {
                let (nameElse, eventsElse) = parsePassage(elseBranch!, story: story)
                passage1 = Passage(name: nameElse, events: eventsElse, eventDelegate: story)
            } else {
                passage1 = Passage(name: nil, events: [], eventDelegate: story)
            }
            story.passages.add(passage1)
            
            return (newPassage, conditionalKey, Condition(parseState(condition, requireTags: false), success: passage0, failure: passage1))
        }
    }
    
    class func parseEvents(_ line : String, preprocessed : [String : Event]) -> [Event] {
        var events : [Event] = []
        
        if line.characters.first == "#" {
            return events
        }
        
        if let conditional = parseConditional(line, processedEvents: preprocessed) {
            events.append(conditional)
        }
            
        else if let event = parseChangeState(line) {
            for change in event {
                events.append(change)
            }
        }
            
        else if let event = parseZeroDelay(line) {
            events.append(event)
        }
            
        else if let event = parseDelay(line) {
            events.append(event)
        }
        else if let event = parseRandom(line) {
            events.append(event)
        }
            
        else if let event = parseAudio(line) {
            events.append(event)
        }
            
        else if let event = parseImage(line) {
            events.append(event)
        }
            
        else if let event = parseKeyboardEntry(line) {
            events.append(event)
        }
            
        else if let event = parseQuestion(line) {
            events.append(event)
        }
            
        else {
            // Assume this is dialogue
            let event = parseState(line, requireTags: true)
            events.append(Dialogue(event))
        }
        
        return events
    }
    
    // :: fightdragon
    class func parseName(_ line : String) -> String? {
        let pattern = "^:: ([a-zA-Z0-9\\s\\?]+)$"
        guard let match = NSRegularExpression.firstMatchWithPattern(pattern, haystack: line) else { return nil }
        return (line as NSString).substring(with: match.rangeAt(1))
    }
    
    // [[fightdragon]]
    class func parseZeroDelay(_ line : String) -> Delay? {
        let pattern = "^\\[\\[([^|]+)?\\]\\]$"
        guard let match = NSRegularExpression.firstMatchWithPattern(pattern, haystack: line) else { return nil }
        return Delay(0, next: (line as NSString).substring(with: match.rangeAt(1)))
    }
    
    // [[delay 31s|fightdragon]]
    class func parseDelay(_ line : String) -> Delay? {
        let pattern = "^\\[\\[delay ([0-9]+)([m|s])\\|([^|]+)?\\]\\]$"
        guard let match = NSRegularExpression.firstMatchWithPattern(pattern, haystack: line) else { return nil }
        guard let time = Int((line as NSString).substring(with: match.rangeAt(1))) else { return nil }
        
        var seconds = TimeInterval(time)
        let scale = (line as NSString).substring(with: match.rangeAt(2))
        let next = (line as NSString).substring(with: match.rangeAt(3))
        
        if scale == "m" {
            seconds = seconds * 60
        }
        
        return Delay(seconds, next: next)
    }
    
    // [[random|fightdragon]]
    class func parseRandom(_ line : String) -> Random? {
        let pattern = "^\\[\\[random\\|(.*)?\\]\\]$"
        guard let match = NSRegularExpression.firstMatchWithPattern(pattern, haystack: line) else { return nil }
        let nextPassagePrefix = parseState((line as NSString).substring(with: match.rangeAt(1)), requireTags: true)
        return Random(nextPassagePrefix)
    }
    
    // <<condition:UUID>>
    class func parseConditional(_ line : String, processedEvents : [String : Event]) -> Condition? {
        let pattern = "^<<conditional:(.*?)>>$"
        guard let match = NSRegularExpression.firstMatchWithPattern(pattern, haystack: line) else { return nil }
        guard let condition = processedEvents[(line as NSString).substring(with: match.rangeAt(1))] as? Condition else { return nil }
        return condition
    }
    
    // <<audio highPitchRinging>>
    class func parseAudio(_ line : String) -> Audio? {
        let pattern = "^<<audio (.*?)>>$"
        guard let match = NSRegularExpression.firstMatchWithPattern(pattern, haystack: line) else { return nil }
        return Audio((line as NSString).substring(with: match.rangeAt(1)))
    }
    
    // <<image highPitchRinging>>
    class func parseImage(_ line : String) -> Image? {
        let pattern = "^<<image (.*?)>>$"
        guard let match = NSRegularExpression.firstMatchWithPattern(pattern, haystack: line) else { return nil }
        return Image((line as NSString).substring(with: match.rangeAt(1)))
    }
    
    // Question ?<<keyboardentry|default [[next]]>>
    class func parseKeyboardEntry(_ line : String) -> KeyboardEntry? {
        let pattern = "^(.*?)<<keyboardentry\\|(.*?) \\[\\[(.*?)\\]\\]>>$"
        guard let match = NSRegularExpression.firstMatchWithPattern(pattern, haystack: line) else { return nil }
        var keyboardType : UIKeyboardType = .default
        
        let keyboardTypeString = (line as NSString).substring(with: match.rangeAt(2))
        switch keyboardTypeString {
        case "numberpad":
            keyboardType = .numberPad
        case "asciicapable":
            keyboardType = .asciiCapable
        case "numbersandpunctuation":
            keyboardType = .numbersAndPunctuation
        case "url":
            keyboardType = .URL
        case "numberpad":
            keyboardType = .numberPad
        case "phonepad":
            keyboardType = .phonePad
        case "namephonepad":
            keyboardType = .namePhonePad
        case "emailaddress":
            keyboardType = .emailAddress
        case "decimalpad":
            keyboardType = .decimalPad
        case "twitter":
            keyboardType = .twitter
        case "websearch":
            keyboardType = .webSearch
        default:
            keyboardType = .default
        }
        
        var question : String? = (line as NSString).substring(with: match.rangeAt(1))
        if question == "" {
            question = nil
        }
        return KeyboardEntry(question, next: (line as NSString).substring(with: match.rangeAt(3)), keyboardType: keyboardType)
    }
    
    // <<$token>>
    class func parseState(_ line : String, requireTags : Bool) -> String {
        var parsedLine = ""
        var index = 0
        // If no tags, make sure that we match a complete tag (ensure that it isn't followed by [A-Za-z0-9_].
        let pattern = requireTags ? "<<\\$([^>]+)>>" : "\\$([^ ><\\$\\\"\\()|]+)(?![A-Za-z0-9_])"
        
        NSRegularExpression.enumerateMatchesWithPattern(pattern, haystack: line) { (result) -> () in
            let old = (line as NSString).substring(with: result!.rangeAt(0))
            let new = "[[" + (line as NSString).substring(with: result!.rangeAt(1)) + "]]"
            
            // Get everything before the token, then the token
            let start = line.characters.index(line.startIndex, offsetBy: index)
            let end = line.characters.index(line.startIndex, offsetBy: result!.rangeAt(0).location)
            let range : Range = Range(start..<end)
            parsedLine = parsedLine + line.substring(with: range) + new
            
            // Advance the index to after this token
            index = result!.rangeAt(0).location + old.characters.count
        }
        
        // Add anything we missed
        parsedLine = parsedLine + (line as NSString).substring(from: index)
        
        return parsedLine
    }
    
    // <<set $token = true>>
    class func parseChangeState(_ line : String) -> [ChangeState]? {
        var results : [ChangeState] = []
        
        let pattern = "<<set\\s*[^>]+>>"
        NSRegularExpression.enumerateMatchesWithPattern(pattern, haystack: line) { (result) -> () in
            // Next, for each <<set ... >>, change the $variable names to [[variable]] names.
            
            let item = (line as NSString).substring(with: result!.rangeAt(0))
            let parsedLine = parseState(item, requireTags: false)
            
            // Finally, create the ChangeState objects
            let setPattern = "<<set\\s*\\[\\[([a-zA-Z0-9]+)\\]\\]\\s*=\\s*(.*?)\\s*>>"
            
            guard let setMatch = NSRegularExpression.firstMatchWithPattern(setPattern, haystack: parsedLine) else {
                Log.Error("Error: Failed to parse setPattern for item: \(item)")
                return
            }
            
            let key = (parsedLine as NSString).substring(with: setMatch.rangeAt(1))
            let value = (parsedLine as NSString).substring(with: setMatch.rangeAt(2))
            
            results.append(ChangeState(key, value: value as AnyObject))
        }
        
        return results.count > 0 ? results : nil
    }
    
    // Do you have a question? <<choice [[title1|next1]]>> | ... | <<choice [[titleN|nextN]]>>
    class func parseQuestion(_ line : String) -> Question? {
        var choices : [Choice] = []
        
        let questionPattern = "^(.*?)<<choice"
        let questionMatch = NSRegularExpression.firstMatchWithPattern(questionPattern, haystack: line)
        var question : String?
        if let questionMatch = questionMatch {
            question = (line as NSString).substring(with: questionMatch.rangeAt(1))
            if question == "" {
                question = nil
            }
        }
        
        let pattern = "<<choice\\s*\\[\\[([^|]+)\\|([^]]*)\\]\\]>>"
        NSRegularExpression.enumerateMatchesWithPattern(pattern, haystack: line) { (result) -> () in
            var title = (line as NSString).substring(with: result!.rangeAt(1))
            title = parseState(title, requireTags: true)
            var next : String? = (line as NSString).substring(with: result!.rangeAt(2))
            if next == "" {
                next = nil
            }
            choices.append(Choice(title, next: next))
        }
        
        if choices.count > 0 {
            return Question(question, choices: choices)
        }
        else {
            return nil
        }
    }
    
    class func decomposeStoryFileIntoPassages(_ storyFile : String) -> [String] {
        let passages = storyFile.components(separatedBy: "\n\n")
        var sanitisedPassages : [String] = []
        for passage in passages {
            var strippedPassage = passage.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
            
            let tagsToRemove = ["<<silently>>", "<<endsilently>>"]
            
            for tag in tagsToRemove {
                strippedPassage = strippedPassage.replacingOccurrences(of: tag, with: "")
            }
            
            if strippedPassage != "" {
                sanitisedPassages.append(strippedPassage)
            }
        }
        return sanitisedPassages
    }
    
    class func decomposePassageIntoLines(_ passage : String) -> [String] {
        let lines = passage.components(separatedBy: "\n")
        var sanitisedLines : [String] = []
        for line in lines {
            let trimmed = line.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
            
            // Remove comments
            if trimmed.characters.count >= 2 && trimmed.substring(to: trimmed.characters.index(trimmed.startIndex, offsetBy: 2)) == "//" {
                continue
            }
            
            if trimmed != "" {
                sanitisedLines.append(trimmed)
            }
        }
        return sanitisedLines
    }
}
