import Foundation

extension NSRegularExpression {
    class func firstMatchWithPattern(_ pattern : String, haystack: String) -> NSTextCheckingResult? {
        let legacyHaystack = haystack as NSString
        do {
            let test = try NSRegularExpression(pattern: pattern, options: [.dotMatchesLineSeparators])
            return test.firstMatch(in: haystack, options: [], range: NSMakeRange(0, legacyHaystack.length))
        }
        catch {
            Log.Error("Error: Failed to find first match")
            return nil
        }
    }
    
    class func enumerateMatchesWithPattern(_ pattern : String, haystack : String, handler : @escaping (_ result : NSTextCheckingResult?) -> ()) {
        let legacyHaystack = haystack as NSString
        do {
            let nameTest = try NSRegularExpression(pattern: pattern, options: [.dotMatchesLineSeparators])
            nameTest.enumerateMatches(in: haystack, options: [], range: NSMakeRange(0, legacyHaystack.length)) { (result : NSTextCheckingResult?, flags : NSRegularExpression.MatchingFlags, stop : UnsafeMutablePointer<ObjCBool>) -> Void in
                handler(result)
            }
        }
        catch {
            Log.Error("Error: Failed to enumerate matches")
            handler(nil)
        }
    }
    
    class func matchesInString(_ pattern : String, haystack : String) -> [NSTextCheckingResult] {
        do {
            let regex = try NSRegularExpression(pattern: pattern, options: [.dotMatchesLineSeparators])
            let results = regex.matches(in: haystack, options: [], range: NSMakeRange(0, (haystack as NSString).length))
            return results
        }
        catch {
            Log.Error("Error: Failed to find matches in string")
            return []
        }
    }
}
