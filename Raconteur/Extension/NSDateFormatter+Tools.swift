import Foundation

private var _standardDateFormatter : DateFormatter?

public extension DateFormatter {
    
    class func standardDateFormatter() -> DateFormatter {
        if _standardDateFormatter == nil {
            _standardDateFormatter = DateFormatter()
            _standardDateFormatter!.dateFormat = "EEE, dd MMM yyyy HH:mm:ss zzz"
        }
        return _standardDateFormatter!
    }
    
}
