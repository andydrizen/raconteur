import Foundation

public extension String {
    func stringByReplacingStateTokens(_ state : [String : AnyObject], quoteStrings: Bool) -> String {
        var output = self
        
        let pattern = "\\[\\[([a-zA-Z0-9_]+)\\]\\]"
        let results = NSRegularExpression.matchesInString(pattern, haystack: self).reversed()
        
        for r in results {
            let token = (self as NSString).substring(with: r.rangeAt(0))
            let tokenValue = (self as NSString).substring(with: r.rangeAt(1))
            
            var stateDictionary = state
            if tokenValue.hasPrefix("system") {
                stateDictionary = Persistence.systemState()
            }
            
            if let resolved = stateDictionary[tokenValue] {
                if let str = resolved as? String, quoteStrings == true {
                    output = output.replacingOccurrences(of: token, with: "\"\(str)\"")
                } else {
                    output = output.replacingOccurrences(of: token, with: "\(resolved)")
                }
            } else {
                output = output.replacingOccurrences(of: token, with: "\(false)")
            }
        }
        
        // Parse rand(<Int>) function
        let randPattern = "rand\\(([0-9]+)\\)"
        if let randMatch = NSRegularExpression.firstMatchWithPattern(randPattern, haystack: output) {
            if let upperBound = Int((output as NSString).substring(with: randMatch.rangeAt(1))) {
                output = (output as NSString).replacingCharacters(in: randMatch.rangeAt(0), with: "\(arc4random_uniform(UInt32(upperBound)))")
            } else {
                preconditionFailure("upperBound is not an Int")
            }
        }
        
        return output
    }
    
    func componentsSeparatedByFirstString(_ token : String) -> [String] {
        var result : [String] = []
        
        if let range = self.range(of: token) {
            let str1 = self.substring(to: range.lowerBound)
            let str2 = self.substring(from: range.upperBound)
            result = [str1, str2]
        }
        
        return result
    }
}
