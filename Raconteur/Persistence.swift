import Foundation

open class Persistence : NSObject {
    
    open class func persistenceDirectory() -> String {
        let documentsPath = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0] as String
        return documentsPath
    }
    
    open class func wipeAll(playerID : String, story : Story) {
        trackDelay(playerID: playerID, delay: nil)
        trackEvent(playerID: playerID, story: story, event: nil)
        wipeState(playerID: playerID)
    }
    
    // MARK: State Tracking
    
    class func filePathForState(playerID : String) -> String {
        let path = URL(string: persistenceDirectory())?.appendingPathComponent("/\(playerID).story.state")
        return (path?.absoluteString)!
    }
    
    class func wipeState(playerID : String) {
        do {
            try FileManager.default.removeItem(atPath: filePathForState(playerID: playerID))
        }
        catch {
            Log.Error("Error: Could not wipe state for player \(playerID)")
        }
    }
    
    open class func state(playerID : String) -> [String : AnyObject]? {
        guard let state = NSKeyedUnarchiver.unarchiveObject(withFile: filePathForState(playerID: playerID)) as? [String:AnyObject] else {
            return nil
        }
        return state
    }
    
    open class func saveState(playerID : String, state : [String : AnyObject]) {
        NSKeyedArchiver.archiveRootObject(state, toFile: filePathForState(playerID: playerID))
    }
    
    
    // MARK: Event Tracking
    
    open class func keyForEventID(playerID : String) -> String {
        return "\(playerID).eventID"
    }
    
    open class func eventID(playerID : String) -> Int? {
        return UserDefaults.standard.value(forKey: keyForEventID(playerID: playerID)) as? Int
    }
    
    open class func trackEvent(playerID : String, story : Story, event : Event?) {
        if let event = event {
            Log.Debug("[\(playerID)] Tracked \(story.passages.indexForEvent(event)): \(event)")
            UserDefaults.standard.setValue(story.passages.indexForEvent(event), forKey: keyForEventID(playerID: playerID))
        } else {
            Log.Debug("[\(playerID)] Cleared Event")
            UserDefaults.standard.removeObject(forKey: keyForEventID(playerID: playerID))
        }
        UserDefaults.standard.synchronize()
    }
    
    // MARK: Event Tracking
    
    open class func keyForDelay(playerID : String) -> String {
        return "\(playerID).delayDateID"
    }
    
    open class func delay(playerID : String) -> Date? {
        if let dateString = UserDefaults.standard.value(forKey: keyForDelay(playerID: playerID)) as? String {
            return DateFormatter.standardDateFormatter().date(from: dateString)
        }
        else {
            return nil
        }
    }
    
    open class func trackDelay(playerID : String, delay : Date?) {
        if let delay = delay {
            Log.Debug("[\(playerID)] Delay Until: \(delay)")
            let dateString = DateFormatter.standardDateFormatter().string(from: delay)
            UserDefaults.standard.setValue(dateString, forKey: keyForDelay(playerID: playerID))
        } else {
            Log.Debug("[\(playerID)] Cleared Delay")
            UserDefaults.standard.removeObject(forKey: keyForDelay(playerID: playerID))
        }
        UserDefaults.standard.synchronize()
    }
    
    // MARK: System Keys
    
    open class func objectForSystemKey(key : String) -> AnyObject? {
        return UserDefaults.standard.object(forKey: key) as AnyObject?
    }
    
    open class func stringForSystemKey(key : String) -> String? {
        return UserDefaults.standard.string(forKey: key)
    }
    
    open class func integerForSystemKey(key : String) -> Int {
        return UserDefaults.standard.integer(forKey: key)
    }
    
    open class func boolForSystemKey(key : String) -> Bool {
        return UserDefaults.standard.bool(forKey: key)
    }
    
    open class func setObject(value : AnyObject, systemKey : String) {
        UserDefaults.standard.set(value, forKey: systemKey)
        UserDefaults.standard.synchronize()
    }
    
    open class func setString(value : String, systemKey : String) {
        UserDefaults.standard.set(value, forKey: systemKey)
        UserDefaults.standard.synchronize()
    }
    
    open class func setInteger(value : Int, systemKey : String) {
        UserDefaults.standard.set(value, forKey: systemKey)
        UserDefaults.standard.synchronize()
    }
    
    open class func setBool(value : Bool, systemKey : String) {
        UserDefaults.standard.set(value, forKey: systemKey)
        UserDefaults.standard.synchronize()
    }
    
    open class func allSystemKeys() -> [String] {
        return Array(UserDefaults.standard.dictionaryRepresentation().keys)
    }
    
    open class func systemState() -> [String : AnyObject] {
        return UserDefaults.standard.dictionaryRepresentation() as [String : AnyObject]
    }
}

public struct PlayerState {}
public struct SystemState {}

extension Story {
    // MARK: Player Keys
    
    public func objectForPlayerKey(key : String) -> AnyObject? {
        return storyState[key]
    }
    
    public func stringForPlayerKey(key : String) -> String? {
        return storyState[key] as? String
    }
    
    public func integerForPlayerKey(key : String) -> Int {
        guard let value = storyState[key] as? Int else {
            return 0
        }
        return value
    }
    
    public func boolForPlayerKey(key : String) -> Bool {
        guard let value = storyState[key] as? Bool else {
            return false
        }
        return value
    }
    
    public func setObject(value : AnyObject, playerKey : String) {
        storyState[playerKey] = value
    }
    
    public func setString(value : String, playerKey : String) {
        storyState[playerKey] = value as AnyObject?
    }
    
    public func setInteger(value : Int, playerKey : String) {
        storyState[playerKey] = value as AnyObject?
    }
    
    public func setBool(value : Bool, playerKey : String) {
        storyState[playerKey] = value as AnyObject?
    }
    
}
