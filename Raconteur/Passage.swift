import Foundation
// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l < r
  case (nil, _?):
    return true
  default:
    return false
  }
}


typealias handler = () -> ()

open class Passages: NSObject {
    var allPassages : [String : Passage] = [:]
    var allEvents : [Event] = []
    
    func add(_ passage : Passage) {
        if let name = passage.name {
            allPassages[name] = passage
        }
        linkPassage(passage)
    }
    
    func passageNamed(_ passageName : String) -> Passage? {
        return allPassages[passageName]
    }
    
    func passagesNamedWithPrefix(_ passagePrefix : String) -> [Passage] {
        let m = Array(allPassages.values).filter() { $0.name!.hasPrefix(passagePrefix) }
        return m
    }
    
    func eventAtIndex(_ index: Int) -> Event {
        return allEvents[index]
    }
    
    open func indexForEvent(_ event: Event) -> Int {
        return allEvents.index(of: event)!
    }
    
    func linkPassage(_ passage : Passage) {
        if passage.events.count == 0 {
            return
        }
        
        passage.events.forEach {
            $0.delegate = passage.eventDelegate
            allEvents.append($0)
        }
        
        // The completion handler for each event in the passage
        // will trigger the next event. However, not every event
        // will necessarily call the completion handler (e.g.
        // questions)
        
        for i in 1...passage.events.count {
            passage.events[i - 1].completionHandler = {[weak self, weak passage] ( passageName : String? ) in
                
                // If the completion handler has told us where to go next...
                if let passageName = passageName {
                    if let event = self?.passageNamed(passageName)?.events.first {
                        event.begin()
                    }
                }
                    
                    // Otherwise go to the next item (if there is one)
                else if i < passage?.events.count {
                    passage?.events[i].begin()
                }
                    
                    // If this event is the child of another, and we've run out
                    // of actions, call the parent's completion handler.
                else if let _ = passage?.events[i - 1].parent {
                    passage?.events[i - 1].parent?.completionHandler!(nil)
                }
                    
                    // Otherwise, this path is a dead end.
                else {
                    guard let deadEndHandler = passage?.deadEndHandler else { return }
                    deadEndHandler()
                }
            }
        }
    }
}

open class Passage: NSObject {
    
    var name : String?
    var deadEndHandler : handler?
    weak var eventDelegate : EventProtocol?
    var events : [Event]

    override open var description : String {
        get {
            return events.description
        }
    }

    init(name: String?, events: [Event], eventDelegate: EventProtocol?) {
        self.name = name
        self.events = events
        self.eventDelegate = eventDelegate
        super.init()
    }

}
