Pod::Spec.new do |s|
  s.name         = "Raconteur"
  s.version      = "2.0.0"
  s.summary      = "Raconteur is a powerful story-telling engine."

  s.description  = <<-DESC
                  Raconteur is a powerful story-telling engine that enables you to transform stories into an interactive and charming engagement with your audience.

                  For generations we've communicated by analogy, metaphor and story. Your ability to convey your thoughts and ideas to others - whether it be software or otherwise - will have a huge impact on your success. Although there will always be early adopters, most of us will not be tempted by a new product without understanding - and connecting with - *your story*.
                   DESC

  s.homepage     = "http://bitbucket.org/andydrizen/raconteur"
  s.license      = { :type => "Copyright", :file => "LICENSE.md" }
  s.author             = { "Andy Drizen" => "andydrizen@gmail.com" }
  s.social_media_url   = "http://twitter.com/AndyDrizen"
  s.platform     = :ios, "9.0"
  s.source       = { :git => "https://andydrizen@bitbucket.org/andydrizen/raconteur.git", :tag => "#{s.version}" }
  s.source_files = "Raconteur/**/*.{swift,h}"
  s.requires_arc = true
end
